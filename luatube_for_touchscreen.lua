--[[
Minetest AutoCook Operating System (MacOS - LOL)
By : AirSThib
On : April 2021
Requirements : Mesecons, Digilines, Digiline TouchScreen, Pipeworks, Technic
--]]

workspace = {}

function update()
    workspace = {
        {
            command="clear",
        },
    }
end

if event.type == "digiline" then
    update()
    digiline_send("touchscreen", workspace)
elseif event.type == "program" then
    digiline_send("touchscreen", workspace)
end
